:- [madilog].
:- [get_value].

baca :- 
    write('Input filename: '),
    read(Filename),
    open(Filename, read, Stream),
    
    read_line_to_codes(Stream, Input),
    string_to_atom(Input, JSON),
    
    parse(JSON, O),
    write(O).