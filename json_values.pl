:- module(values, [
    json_number/2,
    json_boolean/2,
    json_string/2,
    json_array/2,
    json_values/2
]).

:- [utils].

/*
 * json_number/2
 * to check if a value is number
 */
json_number(Value, ValueString) :-
    atomic_list_concat(ValueString, NumberString),
    atom_number(NumberString, Value).

/*
 * json_boolean/2
 * to check if a value is boolean
 */
json_boolean(Value, ValueString) :-
    atomic_list_concat(ValueString, Value),
    must_be(boolean, Value).

/*
 * json_string/2
 * to check if a value is string
 */
json_string(Value, ['"' | Xs]) :-
    last(Xs, '"'),
    except_last(Xs, WithoutLast),
    atomic_list_concat(WithoutLast, Value).

/*
 * json_array/2
 * to check if a value is array
 */
json_array(Values, ['[' | Xs]) :-
    last(Xs, ']'),
    except_last(Xs, WithoutLast),
    json_values(Values, WithoutLast).

/*
 * json_values/2
 * to parse array
 */
% We have > 1 values
json_values([Value | Values], ArrayValues) :-
    length(ArrayValues, Len),
    Len > 0,
    has_many_pair(ArrayValues, 0, true),
    split_json(ArrayValues, FirstValue, SisaValues, [], 0, true),
    json_value(Value, FirstValue),
    json_values(Values, SisaValues).

% We only have 1 value
json_values([Value], ArrayValues) :-    
    length(ArrayValues, Len),
    Len > 0,
    \+ has_many_pair(ArrayValues, 0, true),
    json_value(Value, ArrayValues).
