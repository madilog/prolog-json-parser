:- module(utils, [
    except_last/2,
    split/4,
    has_many_pair/3,
    split_json/6
]).

/*
 * Helper predicates
 * */
% except_last/2
% check if Result is List without last element
except_last([_|[]], []).
except_last([H|T], [H|T2]) :-
    except_last(T, T2).

% split/4
% check if First + [Separator] + Remainder = List
split(List, Separator, First, Remainder) :-
    member(Separator, List),
    append(First, [Separator], FirstWithSeparator),
    append(FirstWithSeparator, Remainder, List).

/*
 * check if a json has has many key value pairs
 */
% active mode
has_many_pair([',' | _], 0, true).
has_many_pair([Char | Chars], Stack, true) :-
    member(Char, ['{', '[']),
    NextStack is Stack + 1,
    has_many_pair(Chars, NextStack, true).
has_many_pair([Char | Chars], Stack, true) :-
    member(Char, ['}', ']']),
    NextStack is Stack - 1,
    has_many_pair(Chars, NextStack, true).
has_many_pair(['"' | Chars], Stack, true) :-
    has_many_pair(Chars, Stack, false).
has_many_pair([Char | Chars], Stack, true) :-
    \+ member(Char, ['{', '}', '[', ']', '"']),
    has_many_pair(Chars, Stack, true).
    
% inactive mode (Di dalam "")
has_many_pair(['"' | Chars], Stack, false) :-
    has_many_pair(Chars, Stack, true).
has_many_pair([Char | Chars], Stack, false) :-
    Char \= '"',
    has_many_pair(Chars, Stack, false).

% 
% split a multipair key-value json
%
split_json([',' | Remainder], First, Remainder, First, 0, true).

% active mode
split_json([Char | Chars], First, Remainder, FirstAccumulator, Stack, true) :-
    member(Char, ['{', '[']),
    NextStack is Stack + 1,
    append(FirstAccumulator, [Char], NewFirstAccumulator),
    split_json(Chars, First, Remainder, NewFirstAccumulator, NextStack, true).

split_json([Char | Chars], First, Remainder, FirstAccumulator, Stack, true) :-
    member(Char, ['}', ']']),
    NextStack is Stack - 1,
    append(FirstAccumulator, [Char], NewFirstAccumulator),
    split_json(Chars, First, Remainder, NewFirstAccumulator, NextStack, true).

split_json(['"' | Chars], First, Remainder, FirstAccumulator, Stack, true) :-
    append(FirstAccumulator, ['"'], NewFirstAccumulator),
    split_json(Chars, First, Remainder, NewFirstAccumulator, Stack, false).

split_json([Char | Chars], First, Remainder, FirstAccumulator, Stack, true) :-
    \+ member(Char, ['{', '}', '[', ']', '"']),
    append(FirstAccumulator, [Char], NewFirstAccumulator),
    split_json(Chars, First, Remainder, NewFirstAccumulator, Stack, true).

% inactive mode
split_json(['"' | Chars], First, Remainder, FirstAccumulator, Stack, false) :-
    append(FirstAccumulator, ['"'], NewFirstAccumulator),
    split_json(Chars, First, Remainder, NewFirstAccumulator, Stack, true).

split_json([Char | Chars], First, Remainder, FirstAccumulator, Stack, false) :-
    append(FirstAccumulator, [Char], NewFirstAccumulator),
    split_json(Chars, First, Remainder, NewFirstAccumulator, Stack, false).