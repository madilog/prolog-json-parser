:- module(get_value, [
    json_get/3
]).

/*
 * Predicate to get value from json
 * */
json_get(json(PasanganKeyValues), KeyCari, ValueHasil) :- 
    cari(PasanganKeyValues, KeyCari, ValueHasil).

cari([json_data(KeyCari, Value) | _], KeyCari, Value) :- !.
cari([json_data(Key, _) | Sisa], KeyCari, Value) :-
    Key \= KeyCari,
    cari(Sisa, KeyCari, Value).