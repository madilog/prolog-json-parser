:- [json_values].
:- [utils].
:- [get_value].

parse(JsonString, Object) :-
    atomic(JsonString),
    atom_chars(JsonString, StringList),
    json_object(Object, StringList),
	!.

% json_object/2 
% To parse a json_object

% case json data
json_object(json([]), ['{', '}']).
json_object(json(Object), ['{' | Xs]) :-
    last(Xs, '}'),
    except_last(Xs, Pairs),
    json_pairs(Object, Pairs),
    !.

% case array data
json_object([], ['[', ']']).
json_object(Values, ['[' | Xs]) :-
    last(Xs, ']'),
    except_last(Xs, ValuesString),
    json_array_data(Values, ValuesString).
                
json_array_data([Value | Values], ValuesString) :-
    has_many_pair(ValuesString, 0, true),
    split_json(ValuesString, FirstValue, SisaValues, [], 0, true),
    process_value(Value, FirstValue),
    json_array_data(Values, SisaValues).

json_array_data([Value], ValuesString) :-
    \+ has_many_pair(ValuesString, 0, true),
    process_value(Value, ValuesString).

process_value(Value, ValueString) :-
    json_object(Value, ValueString).
process_value(Value, ValueString) :-
    json_value(Value, ValueString).

% json_pairs/2
% to parse json elements
json_pairs([Pair | Pairs], PairsString) :-
    has_many_pair(PairsString, 0, true),
    split_json(PairsString, FirstPair, SisaPairs, [], 0, true),
    json_pair(Pair, FirstPair),
    json_pairs(Pairs, SisaPairs),
    !.
json_pairs([Pair], PairString) :-
    json_pair(Pair, PairString),
    !.

% json_pair/2
% to parse a pair (key:value)
json_pair(json_data(Key, Value), PairString) :-
    split(PairString, :, KeyString, ValueString),
    atomic_list_concat(KeyString, Key),
    json_value(Value, ValueString),
    !.

% json_value/2
% to determine value of a json value
json_value(Value, ValueString) :-
    json_number(Value, ValueString).
json_value(Value, ValueString) :-
    json_string(Value, ValueString).
json_value(Value, ValueString) :-
    json_array(Value, ValueString).
json_value(Value,ValueString) :-
    json_object(Value, ValueString).
json_value(Value, ValueString) :-
    json_boolean(Value, ValueString).